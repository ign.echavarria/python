Projets
projet1.py
	Réaliser un jeu de rôle: Créer un jeu de rôle style année 80 auquel il est possiblede jouer dans un terminal.
	Tu vas te battre contre un ennemi, pouvoir attaquer,prendre une potion et te faire attaquer enretour.Faites bien attention, car il y a beaucoup de chosesà gérer.
	Voici les différentes notions qui vont être appliquéeslors de cet examen :
	●	Définir des variables
	●	Réaliser des opérations mathématiques
	●	Récupérer la saisie de l'utilisateur
	●	Utiliser les boucles
	●	Utiliser les modules (notamment le module random)
	●	Faire des vérifications (structures conditionnelles)
	●	Afficher des informations avec les f-string

projet1_2.py
	Le meme jeu que dans le projet1.py, mais une version réalisé avant de regarder le code proposé.
