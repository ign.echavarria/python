import random

health1 = 50
health2 = 50
potions = 3
pass_tour = False

def player1():
    global pass_tour
    if not pass_tour:
        action = input(f"Souhaitez-vous attaquer (1) ou utiliser une potion(2) ?")
        if action == "1":
            attack()
            if health2 <= 0:
                print("Votre ennemi est mort, vous avez gagné.")
                quit()
            else:
                print(f"Votre ennemi à {health2} points de vie.")
                player2()
        elif action == "2":
            potion()
        else:
            print("S'il vous plait, choisir 1 ou 2")
            player1()
    else:
        print("Vous passez votre tour.")
        pass_tour = False
        player2()

def player2():
    attack_power = random.randint(5, 15)
    global health1
    health1 -= attack_power
    if health1 > 0:
        print(f"Votre ennemi a vous attaqué! Il vous à fait {attack_power} points de dégat, il vous restent {health1} points de vie.")
    else:
        print(f"Votre ennemi a vous attaqué avec un force de {attack_power}, malheuresement, vous avez perdu.")
        quit()

def attack():
    global health2
    your_attack = random.randint(5,10)
    health2 -= your_attack
    print(f"Vous avez fait {your_attack} points de dégat.")

def potion():
    global potions
    global health1
    global pass_tour
    if potions > 0:
        health1 += random.randint(15, 50)
        potions -= 1
        print(f"Vous avez bu une potion. Maintenant vous avez {health1} points de vie, et il vous restent {potions} potions.")
        pass_tour = True
        player2()
    else:
        print("Vous n'avez plus de potons.")
        player1()
        

print(f"Bienvenu au jeu violent de la journée. Vous avez {health1} points de vie, et votre ennemi {health2}.")
while health1 > 0:
    player1()
