import random

ENEMY_HEALTH = 50
PLAYER_HEALTH = 50
NUMBER_OF_POTIONS = 3
SKIP_TURN = False

while True:
    if SKIP_TURN:
        print("Vous passez votre tour...")
        SKIP_TURN = False
    else:
        user_choice = input("Tapez 1 pour attaquer ou 2 pour prendre une potion: ")
        while user_choice not in ["1", "2"]:
            user_choice = input("Vous avez saissi une valeur invalide. Tapez 1 pour attaquer ou 2 pour prendre une potion")
        if user_choice == "1":
            your_attack = random.randint(5, 10)
            ENEMY_HEALTH -= your_attack
            print(f"Vous avez infligé {your_attack} points de dégat à l'ennemi.")
        elif NUMBER_OF_POTIONS > 0:
            potion_health = random.randint(15, 50)
            PLAYER_HEALTH += potion_health
            NUMBER_OF_POTIONS -= 1
            SKIP_TURN = True
            print(f"Vous recuperez {potion_health} points de vie")
    if ENEMY_HEALTH > 0:        
        enemy_attack = random.randint(5, 15)
        PLAYER_HEALTH -= enemy_attack
        print(f"L'ennemi à vous infligé {enemy_attack} points de dégat.")
        if PLAYER_HEALTH <= 0:
            print(f"Vous avez perdu, votre ennemi gagne avec {ENEMY_HEALTH} points de vie restantes.")
            break
    else:
        print(f"Vous avez gagné avec {PLAYER_HEALTH} points de vie restantes.")
        break
