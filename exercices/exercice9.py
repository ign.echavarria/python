continuer = "o"
operateurs = ("+", "-", "*", "/")

def addition(a, b):
    c = a + b
    return c

def reste(a, b):
    c = a - b
    return c

def multiplication(a, b):
    c = a * b
    return c

def division(a, b):
    c = a / b
    return c

while continuer == "o":
    operation = ""
    chiffre1 = int(input("Choisissez votre premier chiffre: "))
    chiffre2 = int(input("Choisissez votre deuxieme chiffre: "))
    while not operation in operateurs:    
        operation = input("Tappez + - * ou / pour additionner, rester, multiplier ou diviser vos chiffres: ")
        if operation == "+":
            resultat = addition(chiffre1, chiffre2)
        elif operation == "-":
            resultat = reste(chiffre1, chiffre2)
        elif operation == "*":
            resultat = multiplication(chiffre1, chiffre2)
        elif operation == "/":
            resultat = division(chiffre1, chiffre2)
        else:
            print("Une erreur est parvenu.")
            break
    print(f"Le résultat de {chiffre1} {operation} {chiffre2} est égal à {resultat}.")
    continuer = input("Voulez-vous faire une autre operation? o/n ").lower()
