answer = ""
list_films = []
list_films_notes = []
t= ()
while answer != "n":
    answer = ""
    film = ""
    note = 0
    while film == "":
        film = input("Saisissez le nom du film à ajouter: ")
    if film not in list_films:
        list_films.append(film)
        note = 0
        while note == 0:
            note = input(f"Saissisez la note pour le film '{film}': ")
            try:
                note = int(note)
                if note >= 1 and note <= 5:
                    t = (film, note)
                    list_films_notes.append(t)
                    break
                else:
                    print("La note doit etre un entier entre 1 et 5.")
                    note = 0
            except:
                print("La note doit etre un entier entre 1 et 5.")
                note = 0
    else:
        print("Le film qui vous avez saissi est déjà dans la liste")
    while not (answer == "n" or answer == "o"):
            answer = input("Voulez vous ajouter un autre film à la liste?(o/n)").lower()
list_films.sort()
list_films_notes.sort()
print(f"La liste des films est {list_films}")
print(f"La liste des films notés est {list_films_notes}")
tuplemax = tuplemin = list_films_notes[0]
for tupl in list_films_notes:
    if tupl[1] > tuplemax[1]:
        tuplemax = tupl
    elif tupl[1] < tuplemin[1]:
        tuplemin = tupl
print(tuplemax, tuplemin)
