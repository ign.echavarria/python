import os

structure = {
        "Musique": ["Rock", "Jazz", "Pop"],
        "Documents": ["Factures", "Travail", "Maison"],
        "Images": ["Vacances", "Famille"],
        "Videos": ["Chat", "Facebook"]
        }
def creation_dossiers(dictionnaire):
    for key in dictionnaire:
        for sub_dossier in dictionnaire.get(key):
            path = os.path.join(key, sub_dossier)
            os.makedirs(path)

creation_dossiers(structure)
