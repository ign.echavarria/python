class Livre:
    def __init__(self, nom, pages, prix):
        self.nom = nom
        self.pages = pages
        self.prix = prix
    def print_livre(self):
        print(f"Le livre {self.nom} a {self.pages} pages et coute {self.prix}€")

livre_HP = Livre("Harry Potter", 300, 10.99)
livre_LOTR = Livre("Le Seigneur des Anneaux", 400, 13.99)
livres = ["livre_HP", "livre_LOTR"]
livre_LOTR.print_livre()
livre_HP.print_livre()
