import sys
import os
import json

path = os.getcwd()+"/liste_de_courses.json"
if not os.path.exists(path):
    print(f"Le fichier {path} n'existe pas, il sera crée par le programme.")
    LISTE = []
else:
    with open (path, "r") as f:
        data = json.load(f)
    LISTE = list(data)
if LISTE == []:
    print("Votre liste de courses est vide.")
else:
    print("Votre liste de courses contient: ")
    for cle in LISTE:
        print(cle)
MENU = """Choisissez parmi les 5 options suivantes :
1: Ajouter un élément à la liste
2: Retirer un élément de la liste
3: Afficher la liste
4: Vider la liste
5: Quitter
? Votre choix : """
MENU_CHOICES = ["1", "2", "3", "4", "5"]
while True:
    user_choice = input(MENU)
    if user_choice not in MENU_CHOICES:
        print("Veuillez choisir une option valide...")
        continue
    if user_choice == "1":  # Ajouter un élément
        item = input("Entrez le nom d'un élément à ajouter à la liste de courses : ")
        LISTE.append(item)
        print(f"L'élément {item} a bien été ajouté à la liste.")
    elif user_choice == "2":  # Retirer un élément
        item = input("Entrez le nom d'un élément à retirer de la liste de courses : ")
        if item in LISTE:
            LISTE.remove(item)
            print(f"L'élément {item} a bien été supprimé de la liste.")
        else:
            print(f"L'élément {item} n'est pas dans la liste.")
    elif user_choice == "3":  # Afficher la liste
        if LISTE:
            print("Voici le contenu de votre liste :")
            for i, item in enumerate(LISTE, 1):
                print(f"{i}. {item}")
        else:
            print("Votre liste ne contient aucun élément.")
    elif user_choice == "4":  # Vider la liste
        LISTE.clear()
        print("La liste a été vidée de son contenu.")
    elif user_choice == "5":  # Quitter
        with open("liste_de_courses.json", "w") as f:
            json.dump(LISTE, f)
        print("À bientôt !")
        sys.exit()
