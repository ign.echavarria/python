liste_de_courses = []
quitter = False
while quitter == False:
	print("Chossisez votre option:\n1 - Ajouter un élément à la liste de courses.\n2 - Retirer un élément de la liste de courses.\n3 - Afficher les éléments de la liste de courses\n4 - Vider la liste de courses.\n5 - Quitter le programme.")
	option = input()
	try:
		option = int(option)
		if option == 1:
			liste_de_courses.append(input("Saissisez l'élément à ajouter: "))
		elif option == 2:
			liste_de_courses.remove(input("Saissisez l'élément à retirer de la liste: "))
		elif option == 3:
			print(liste_de_courses)
		elif option == 4:
			liste_de_courses=[]
		elif option == 5:
			quitter = True
	except:
		print("L'option que vous aviez choisi n'est pas valide, réessayez.")
